FROM ubuntu:16.04
MAINTAINER Jorge Suit <josp.jorge at gmail.com>

LABEL description="A linux C++ build environment with vtk, itk & vmtk"

RUN apt-get update && apt-get install -y \
  automake \
  autoconf \
  gcc \
  cmake \
  g++ \
  git \
  libboost-all-dev \
  libglu1-mesa-dev \
  libgtest-dev \
  tcl \
  tcl-dev \
  tk \
  tk-dev \
  libtk-img \
  tdom \
  bwidget \
  tcllib \
  tklib \
  python \
  python-dev \
  python-numpy \
  wget && \
  mkdir /src && \
  mkdir -p /build/ITK && \
  mkdir -p /build/VTK && \
  mkdir -p /build/vmtk && \
  mkdir -p /build/gtest && \
  cd /build/gtest && \
  cmake /usr/src/gtest -DCMAKE_BUILD_TYPE=Release && \
  make && cp libgtest*.a /usr/lib && \
  cd /src && \
  wget https://downloads.sourceforge.net/project/tktreectrl/tktreectrl/tktreectrl-2.4.1/tktreectrl-2.4.1.tar.gz && \
  tar zxf tktreectrl-2.4.1.tar.gz && \
  wget https://downloads.sourceforge.net/project/itk/itk/4.12/InsightToolkit-4.12.2.tar.gz && \
  tar zxf InsightToolkit-4.12.2.tar.gz && \
  wget http://www.vtk.org/files/release/8.0/VTK-8.0.1.tar.gz && \
  tar zxf VTK-8.0.1.tar.gz && \
  git clone https://github.com/vmtk/vmtk.git && \
  cd /src/tktreectrl-2.4.1 && \
  ./configure && make && make install && \
  cd /build/VTK && \
  cmake /src/VTK-8.0.1 -DCMAKE_BUILD_TYPE=Release -DBUILD_DOCUMENTATION=OFF \
        -DBUILD_SHARED_LIBS=ON -DBUILD_TESTING=OFF -DVTK_WRAP_PYTHON=ON \
        -DVTK_WRAP_TCL=ON -DVTK_Group_Tk=ON -DVTK_USE_TK=ON \
        -DCMAKE_INSTALL_PREFIX=/usr && \
  make -j8 && make install && \
  ln -s /usr/lib/python2.7/site-packages/vtk \
     /usr/lib/python2.7/dist-packages/. && \
  cd /build/ITK && \
  cmake /src/InsightToolkit-4.12.2 -DCMAKE_BUILD_TYPE=Release \
        -DBUILD_DOCUMENTATION=OFF -DBUILD_EXAMPLES=OFF -DBUILD_SHARED_LIBS=ON \
        -DBUILD_TESTING=OFF -DModule_ITKVtkGlue=ON -DModule_ITKReview=ON \
        -DCMAKE_INSTALL_PREFIX=/usr && \
  make -j8 && make install && \
  cd /build/vmtk && \
  cmake /src/vmtk -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr \
        -DUSE_SYSTEM_ITK=ON -DUSE_SYSTEM_VTK=ON && \
  make -j8 && \
  cp -r Install/bin Install/include Install/lib /usr && \
  ln -s /usr/lib/python2.7/site-packages/vmtk \
     /usr/lib/python2.7/dist-packages/. && \
  rm -rf /build && \
  rm -rf /src && \
  apt-get clean autoclean autoremove -y
