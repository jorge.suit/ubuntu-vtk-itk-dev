# A C++ env with VTK & ITK development libraries

Here we provide a dockerfile based on ubuntu 16.04 to create a
container including the following development libraries & tools:

* cmake
* g++
* liboost
* libglu1-mesa
* libgtest-dev
* tcl
* tk
* tcllib, bwidget, tktreectrl, tdom, tk-img, tklib
* vtk 8.0.1 + TCL wrapping
* itk 4.12.2

### Build the image

```
docker build . -t josp/ubuntu-vtk-itk-dev:8.0-4.12
```

```
docker tag josp/ubuntu-vtk-itk-dev:8.0-4.12 josp/ubuntu-vtk-itk-dev:latest
```

### Publish the image

```
docker push josp/ubuntu-vtk-itk-dev:8.0-4.12
```

### Run container

You can run the container using the run docker command. Below are some
examples:

**Run the container sharing the X socket**

```
docker run -it -e DISPLAY=$DISPLAY \
  --mount type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix \
  josp/ubuntu-vtk-itk-dev:8.0-4.12 /bin/bash
```

**sharing also the current working directory**

```
docker run -it -e DISPLAY=$DISPLAY \
  --mount type=bind,source="$(pwd)",target=/source \
  --mount type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix \
  josp/ubuntu-vtk-itk-dev:8.0-4.12 /bin/bash
```
